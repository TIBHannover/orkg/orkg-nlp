from standoffreader import process_standoff_files, write_to_conll_allennlp, create_spacy_tokenizer, parse_entities, write_entities
import glob
import os
import numpy as np
from shutil import copyfile

np.random.seed(4578)

ignore_entities = ["Result", "Task", "Object"]

origin_path = "data/dataset_v2/origin"
folds_path = "data/dataset_v2/folds"

def copy_file_pairs(files, dest):
    os.makedirs(dest, exist_ok=True)
    for txt, ann in files:
        print(f"copy {txt} to {dest}")
        copyfile(txt, os.path.join(dest, os.path.basename(txt)))
        print(f"copy {ann} to {dest}")

        # filter entities in ann files
        with open(ann, "r", encoding="utf-8") as ann_fr:
            ann_text = ann_fr.read()
        entities = parse_entities(ann_text, ignore_entities=ignore_entities)
        entities_str = write_entities(entities)
        with open(os.path.join(dest, os.path.basename(ann)), "w", encoding="utf-8") as ann_f:
            ann_f.write(entities_str)


def get_elems(elems, start, end):
    l = len(elems)
    if start < l and end < l:
        return elems[start:end]
    if start < l and end >= l:
        return elems[start:] + elems[0:end % l]
    if start >= l and end >= l:
        return elems[start % l:end % l]

    raise IOError(f'start={start}, end={end}, len={l}')


def get_standoff_examples(path, spacy_nlp):
    docs = process_standoff_files(path, keep_only_one_label=True, spacy_nlp=spacy_nlp, ignore_entities=ignore_entities)
    return docs

def write_file(folder, filename, text):
    os.makedirs(folder, exist_ok=True)
    # append to the file
    with open(os.path.join(folder, filename), "a", encoding="utf-8") as f:
        f.write(text)

def write_to_conll(path_out, fold, split, domain_from, domain_to, spacy_nlp):
    examples = get_standoff_examples(os.path.join(path_out, f'fold_{fold}', 'standoff', split, domain_from), spacy_nlp)
    path = os.path.join(path_out, f'fold_{fold}', 'conll', split, domain_to)
    write_file(path, f"{split}.txt", write_to_conll_allennlp(examples))

def create_splits(path, path_out, folds):
    # instantiate tokenizer once as it is expensive
    print("loading tokenizer...")
    spacy_nlp = create_spacy_tokenizer()

    for d in glob.glob(f"{path}/*/"):
        domain = os.path.basename(os.path.normpath(d))
        print(f'Processing domain {domain}')

        txt_files = list(sorted(glob.glob(f'{d}/*.txt')))
        ann_files = list(sorted(glob.glob(f'{d}/*.ann')))
        assert len(txt_files) == len(ann_files)

        files = list(zip(txt_files, ann_files))
        np.random.shuffle(files)
        print(files)

        for fold in range(folds):
            split_0 = 0 + fold
            split_1 = 8 + fold
            split_2 = 9 + fold
            split_3 = 11 + fold
            assert len(files) == 11

            train_filenames = get_elems(files, split_0, split_1)
            dev_filenames = get_elems(files, split_1, split_2)
            test_filenames = get_elems(files, split_2, split_3)

            assert set(train_filenames).isdisjoint(dev_filenames)
            assert set(train_filenames).isdisjoint(test_filenames)
            assert set(test_filenames).isdisjoint(dev_filenames)

            # copy standoff files for each domain
            copy_file_pairs(train_filenames, os.path.join(path_out, f'fold_{fold}', 'standoff', 'train', domain))
            copy_file_pairs(dev_filenames, os.path.join(path_out, f'fold_{fold}', 'standoff', 'dev', domain))
            copy_file_pairs(test_filenames, os.path.join(path_out, f'fold_{fold}', 'standoff', 'test', domain))

            # copy standoff files for overall
            copy_file_pairs(train_filenames, os.path.join(path_out, f'fold_{fold}', 'standoff', 'train', 'overall'))
            copy_file_pairs(dev_filenames, os.path.join(path_out, f'fold_{fold}', 'standoff', 'dev', 'overall'))
            copy_file_pairs(test_filenames, os.path.join(path_out, f'fold_{fold}', 'standoff', 'test', 'overall'))

            #create conll data for each domain
            write_to_conll(path_out, fold, "train", domain_from=domain, domain_to=domain, spacy_nlp=spacy_nlp)
            write_to_conll(path_out, fold, "test", domain_from=domain, domain_to=domain, spacy_nlp=spacy_nlp)
            write_to_conll(path_out, fold, "dev", domain_from=domain, domain_to=domain, spacy_nlp=spacy_nlp)

            # create conll data for overall (appending to the file)
            write_to_conll(path_out, fold, "train", domain_from=domain, domain_to='overall', spacy_nlp=spacy_nlp)
            write_to_conll(path_out, fold, "test", domain_from=domain, domain_to='overall', spacy_nlp=spacy_nlp)
            write_to_conll(path_out, fold, "dev", domain_from=domain, domain_to='overall', spacy_nlp=spacy_nlp)




create_splits(origin_path, folds_path, 11)





