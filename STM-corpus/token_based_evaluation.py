import numpy as np
from sklearn.metrics import precision_recall_fscore_support, classification_report, confusion_matrix, accuracy_score
import pandas as pd


def calc_classification_metrics_from_file(path, labels):
    df = pd.read_csv(path, sep="\t")
    y_true = df["gold"].values
    y_predicted = df["predicted"].values
    return calc_classification_metrics(y_true, y_predicted, labels)


def calc_classification_metrics(y_true, y_predicted, labels):
    def norm(val):
        return round(val, 2)
    _, _, macro_f1, _ = precision_recall_fscore_support(y_true, y_predicted, average='macro')
    _, _, micro_f1, _ = precision_recall_fscore_support(y_true, y_predicted, average='micro')
    _, _, weighted_f1, _ = precision_recall_fscore_support(y_true, y_predicted, average='weighted')
    acc = accuracy_score(y_true, y_predicted)

    # class_report = classification_report(y_true, y_predicted, digits=4)
    confusion = confusion_matrix(y_true, y_predicted, labels=labels)
    # normalize confusion matrix
    confusion = np.around(confusion.astype('float') / confusion.sum(axis=1)[:, np.newaxis] * 100, 2).tolist()
    confusion_abs = confusion_matrix(y_true, y_predicted, labels=labels).astype('int').tolist()
    return {"accuracy": acc, "macro-f1": macro_f1, "micro-f1": micro_f1,
            "weighted-f1": weighted_f1,
            "confusion_rel": confusion, "confusion_abs": confusion_abs, "labels": labels}

