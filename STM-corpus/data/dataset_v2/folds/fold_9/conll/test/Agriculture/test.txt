-DOCSTART- (S0168945213001805)

Biomass	0,7	O	B-Material
is	8,10	O	O
a	11,12	O	O
prime	13,18	O	O
target	19,25	O	O
for	26,29	O	O
genetic	30,37	O	B-Process
engineering	38,49	O	I-Process
in	50,52	O	I-Process
forestry	53,61	O	I-Process
because	62,69	O	O
increased	70,79	O	B-Process
biomass	80,87	O	B-Material
yield	88,93	O	I-Material
will	94,98	O	O
benefit	99,106	O	O
most	107,111	O	B-Process
downstream	112,122	O	I-Process
applications	123,135	O	I-Process
such	136,140	O	O
as	141,143	O	O
timber	144,150	O	B-Material
,	150,151	O	I-Material
fiber	152,157	O	I-Material
,	157,158	O	I-Material
pulp	159,163	O	I-Material
,	163,164	O	I-Material
paper	165,170	O	I-Material
,	170,171	O	I-Material
and	172,175	O	I-Material
bioenergy	176,185	O	I-Material
production	186,196	O	B-Process
.	196,197	O	O

Transgenesis	198,210	O	B-Process
can	211,214	O	O
increase	215,223	O	B-Process
biomass	224,231	O	B-Material
by	232,234	O	O
improving	235,244	O	O
resource	245,253	O	B-Process
acquisition	254,265	O	I-Process
and	266,269	O	I-Process
product	270,277	O	I-Process
utilization	278,289	O	I-Process
and	290,293	O	O
by	294,296	O	O
enhancing	297,306	O	O
competitive	307,318	O	B-Process
ability	319,326	O	I-Process
for	327,330	O	O
solar	331,336	O	B-Material
energy	337,343	O	I-Material
,	343,344	O	I-Material
water	345,350	O	I-Material
,	350,351	O	I-Material
and	352,355	O	I-Material
mineral	356,363	O	I-Material
nutrients	364,373	O	I-Material
.	373,374	O	O

Transgenes	375,385	O	B-Material
that	386,390	O	O
affect	391,397	O	O
juvenility	398,408	O	B-Process
,	408,409	O	O
winter	410,416	O	B-Process
dormancy	417,425	O	I-Process
,	425,426	O	O
and	427,430	O	O
flowering	431,440	O	B-Process
have	441,445	O	O
been	446,450	O	O
shown	451,456	O	O
to	457,459	O	O
influence	460,469	O	O
biomass	470,477	O	B-Material
as	478,480	O	O
well	481,485	O	O
.	485,486	O	O

Transgenic	487,497	O	B-Process
approaches	498,508	O	I-Process
have	509,513	O	O
increased	514,523	O	B-Process
yield	524,529	O	B-Process
potential	530,539	O	I-Process
by	540,542	O	O
mitigating	543,553	O	B-Process
the	554,557	O	B-Process
adverse	558,565	O	I-Process
effects	566,573	O	I-Process
of	574,576	O	O
prevailing	577,587	O	O
stress	588,594	O	B-Data
factors	595,602	O	I-Data
in	603,605	O	O
the	606,609	O	B-Material
environment	610,621	O	I-Material
.	621,622	O	O

Simultaneous	623,635	O	O
introduction	636,648	O	B-Process
of	649,651	O	O
multiple	652,660	O	B-Material
genes	661,666	O	I-Material
for	667,670	O	O
resistance	671,681	O	B-Process
to	682,684	O	O
various	685,692	O	B-Data
stress	693,699	O	I-Data
factors	700,707	O	I-Data
into	708,712	O	O
trees	713,718	O	B-Material
may	719,722	O	O
help	723,727	O	O
forest	728,734	O	B-Material
trees	735,740	O	I-Material
cope	741,745	O	B-Process
with	746,750	O	O
multiple	751,759	O	B-Material
or	760,762	O	I-Material
changing	763,771	O	I-Material
environments	772,784	O	I-Material
.	784,785	O	O

We	786,788	O	O
propose	789,796	O	O
multi	797,802	O	B-Method
-	802,803	O	I-Method
trait	803,808	O	I-Method
engineering	809,820	O	I-Method
for	821,824	O	O
tree	825,829	O	B-Material
crops	830,835	O	I-Material
,	835,836	O	O
simultaneously	837,851	O	O
deploying	852,861	O	B-Process
multiple	862,870	O	B-Material
independent	871,882	O	I-Material
genes	883,888	O	I-Material
to	889,891	O	O
address	892,899	O	O
a	900,901	O	B-Data
set	902,905	O	I-Data
of	906,908	O	I-Data
genetically	909,920	O	I-Data
uncorrelated	921,933	O	I-Data
traits	934,940	O	I-Data
that	941,945	O	O
are	946,949	O	O
important	950,959	O	O
for	960,963	O	O
crop	964,968	O	B-Process
improvement	969,980	O	I-Process
.	980,981	O	O

This	982,986	O	B-Method
strategy	987,995	O	I-Method
increases	996,1005	O	O
the	1006,1009	O	B-Data
probability	1010,1021	O	I-Data
of	1022,1024	O	O
unpredictable	1025,1038	O	B-Process
(	1039,1040	O	I-Process
synergistic	1040,1051	O	I-Process
or	1052,1054	O	I-Process
detrimental	1055,1066	O	I-Process
)	1066,1067	O	I-Process
interactions	1068,1080	O	I-Process
that	1081,1085	O	O
may	1086,1089	O	O
substantially	1090,1103	O	O
affect	1104,1110	O	O
the	1111,1114	O	B-Data
overall	1115,1122	O	I-Data
phenotype	1123,1132	O	I-Data
and	1133,1136	O	O
its	1137,1140	O	B-Data
long	1141,1145	O	I-Data
-	1145,1146	O	I-Data
term	1146,1150	O	I-Data
performance	1151,1162	O	I-Data
.	1162,1163	O	O

The	1164,1167	O	O
very	1168,1172	O	O
limited	1173,1180	O	O
ability	1181,1188	O	O
to	1189,1191	O	O
predict	1192,1199	O	B-Process
the	1200,1203	O	B-Process
physiological	1204,1217	O	I-Process
processes	1218,1227	O	I-Process
that	1228,1232	O	O
may	1233,1236	O	O
be	1237,1239	O	O
impacted	1240,1248	O	O
by	1249,1251	O	O
such	1252,1256	O	B-Method
a	1257,1258	O	I-Method
strategy	1259,1267	O	I-Method
requires	1268,1276	O	O
vigilance	1277,1286	O	B-Process
and	1287,1290	O	I-Process
care	1291,1295	O	I-Process
during	1296,1302	O	O
implementation	1303,1317	O	O
.	1317,1318	O	O

Hence	1319,1324	O	O
,	1324,1325	O	O
we	1326,1328	O	O
recommend	1329,1338	O	O
close	1339,1344	O	B-Process
monitoring	1345,1355	O	I-Process
of	1356,1358	O	O
the	1359,1362	O	B-Material
resultant	1363,1372	O	I-Material
transgenic	1373,1383	O	I-Material
genotypes	1384,1393	O	I-Material
in	1394,1396	O	O
multi	1397,1402	O	B-Process
-	1402,1403	O	I-Process
year	1403,1407	O	I-Process
,	1407,1408	O	I-Process
multi	1409,1414	O	I-Process
-	1414,1415	O	I-Process
location	1415,1423	O	I-Process
field	1424,1429	O	I-Process
trials	1430,1436	O	I-Process
.	1436,1437	O	O

-DOCSTART- (S0378112713005288)

In	0,2	O	O
upland	3,9	O	O
areas	10,15	O	O
of	16,18	O	O
Great	19,24	O	O
Britain	25,32	O	O
,	32,33	O	O
large	34,39	O	B-Material
tracts	40,46	O	I-Material
of	47,49	O	I-Material
non	50,53	O	I-Material
-	53,54	O	I-Material
native	54,60	O	I-Material
conifer	61,68	O	I-Material
plantations	69,80	O	I-Material
have	81,85	O	O
been	86,90	O	O
established	91,102	O	B-Process
on	103,105	O	O
poor	106,110	O	B-Data
quality	111,118	O	I-Data
agricultural	119,131	O	B-Material
land	132,136	O	I-Material
.	136,137	O	O

There	138,143	O	O
is	144,146	O	O
now	147,150	O	O
considerable	151,163	O	O
interest	164,172	O	O
in	173,175	O	O
the	176,179	O	B-Process
conversion	180,190	O	I-Process
of	191,193	O	O
some	194,198	O	O
of	199,201	O	O
these	202,207	O	B-Material
plantations	208,219	O	I-Material
to	220,222	O	O
a	223,224	O	B-Material
more	225,229	O	I-Material
natural	230,237	O	I-Material
woodland	238,246	O	I-Material
comprised	247,256	O	O
of	257,259	O	O
native	260,266	O	B-Material
tree	267,271	O	I-Material
species	272,279	O	I-Material
.	279,280	O	O

We	281,283	O	O
studied	284,291	O	O
the	292,295	O	B-Process
tree	296,300	O	I-Process
regeneration	301,313	O	I-Process
and	314,317	O	O
ground	318,324	O	B-Material
flora	325,330	O	I-Material
on	331,333	O	O
15	334,336	O	B-Data
upland	337,343	O	B-Material
sites	344,349	O	I-Material
(	350,351	O	O
altitudes	351,360	O	B-Data
ranging	361,368	O	I-Data
from	369,373	O	I-Data
120	374,377	O	I-Data
m	377,378	O	I-Data
to	379,381	O	I-Data
380	382,385	O	I-Data
m	385,386	O	I-Data
above	387,392	O	O
sea	393,396	O	B-Data
level	397,402	O	I-Data
)	402,403	O	O
that	404,408	O	O
had	409,412	O	O
been	413,417	O	O
clearfelled	418,429	O	B-Process
of	430,432	O	O
conifers	433,441	O	B-Material
.	441,442	O	O

Regeneration	443,455	O	B-Process
of	456,458	O	O
native	459,465	O	B-Material
tree	466,470	O	I-Material
species	471,478	O	I-Material
was	479,482	O	O
successful	483,493	O	O
where	494,499	O	O
a	500,501	O	B-Material
clearcut	502,510	O	I-Material
site	511,515	O	I-Material
was	516,519	O	O
adjacent	520,528	O	O
to	529,531	O	O
mature	532,538	O	B-Material
native	539,545	O	I-Material
trees	546,551	O	I-Material
,	551,552	O	O
which	553,558	O	O
acted	559,564	O	O
as	565,567	O	O
a	568,569	O	B-Material
seed	570,574	O	I-Material
source	575,581	O	I-Material
.	581,582	O	O

Mean	583,587	O	B-Data
regeneration	588,600	O	I-Data
densities	601,610	O	I-Data
of	611,613	O	O
native	614,620	O	B-Material
tree	621,625	O	I-Material
species	626,633	O	I-Material
on	634,636	O	I-Material
clearcut	637,645	O	I-Material
sites	646,651	O	I-Material
were	652,656	O	O
typically	657,666	O	O
greater	667,674	O	B-Data
than	675,679	O	I-Data
1000stems	680,689	O	I-Data
/	689,690	O	I-Data
ha	690,692	O	I-Data
,	692,693	O	O
exceeding	694,703	O	O
minimum	704,711	O	O
recommended	712,723	O	O
planting	724,732	O	B-Data
densities	733,742	O	I-Data
for	743,746	O	O
the	747,750	O	B-Process
establishment	751,764	O	I-Process
of	765,767	O	O
new	768,771	O	B-Material
native	772,778	O	I-Material
woodland	779,787	O	I-Material
.	787,788	O	O

Whilst	789,795	O	O
10	796,798	O	B-Data
native	799,805	O	B-Material
woody	806,811	O	I-Material
tree	812,816	O	I-Material
species	817,824	O	I-Material
were	825,829	O	O
recorded	830,838	O	B-Process
,	838,839	O	O
the	840,843	O	B-Process
regeneration	844,856	O	I-Process
was	857,860	O	O
dominated	861,870	O	O
by	871,873	O	O
birch	874,879	O	B-Material
species	880,887	O	I-Material
.	887,888	O	O

Regeneration	889,901	O	B-Data
densities	902,911	O	I-Data
were	912,916	O	O
significantly	917,930	O	O
higher	931,937	O	O
on	938,940	O	O
clearcut	941,949	O	B-Material
sites	950,955	O	I-Material
than	956,960	O	O
on	961,963	O	O
adjacent	964,972	O	B-Material
areas	973,978	O	I-Material
of	979,981	O	O
unplanted	982,991	O	B-Material
moorland	992,1000	O	I-Material
,	1000,1001	O	O
probably	1002,1010	O	O
due	1011,1014	O	O
to	1015,1017	O	O
the	1018,1021	O	O
lack	1022,1026	O	O
of	1027,1029	O	O
a	1030,1031	O	B-Material
dense	1032,1037	O	I-Material
ground	1038,1044	O	I-Material
flora	1045,1050	O	I-Material
following	1051,1060	O	O
the	1061,1064	O	B-Process
clearfelling	1065,1077	O	I-Process
operations	1078,1088	O	I-Process
.	1088,1089	O	O

Our	1090,1093	O	O
results	1094,1101	O	O
indicate	1102,1110	O	O
that	1111,1115	O	O
where	1116,1121	O	O
local	1122,1127	O	B-Material
native	1128,1134	O	I-Material
seed	1135,1139	O	I-Material
sources	1140,1147	O	I-Material
exist	1148,1153	O	O
,	1153,1154	O	O
clearfelling	1155,1167	O	B-Process
upland	1168,1174	O	B-Material
conifer	1175,1182	O	I-Material
plantation	1183,1193	O	I-Material
sites	1194,1199	O	I-Material
to	1200,1202	O	O
allow	1203,1208	O	O
natural	1209,1216	O	B-Process
regeneration	1217,1229	O	I-Process
has	1230,1233	O	O
the	1234,1237	O	O
potential	1238,1247	O	O
to	1248,1250	O	O
be	1251,1253	O	O
an	1254,1256	O	O
effective	1257,1266	O	O
method	1267,1273	O	O
of	1274,1276	O	O
establishing	1277,1289	O	B-Process
native	1290,1296	O	B-Material
woodland	1297,1305	O	I-Material
.	1305,1306	O	O

