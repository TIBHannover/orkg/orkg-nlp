-DOCSTART- (S0022247X13008238)

In	0,2	O	O
this	3,7	O	O
note	8,12	O	O
we	13,15	O	O
show	16,20	O	O
how	21,24	O	O
to	25,27	O	O
include	28,35	O	O
low	36,39	O	B-Data
order	40,45	O	I-Data
terms	46,51	O	I-Data
in	52,54	O	O
the	55,58	O	B-Data
C∞	59,61	O	I-Data
well	62,66	O	I-Data
-	66,67	O	I-Data
posedness	67,76	O	I-Data
results	77,84	O	I-Data
for	85,88	O	O
weakly	89,95	O	B-Process
hyperbolic	96,106	O	I-Process
equations	107,116	O	I-Process
with	117,121	O	O
analytic	122,130	O	B-Data
time	131,135	O	I-Data
-	135,136	O	I-Data
dependent	136,145	O	I-Data
coefficients	146,158	O	I-Data
.	158,159	O	O

This	160,164	O	O
is	165,167	O	O
achieved	168,176	O	O
by	177,179	O	O
doing	180,185	O	O
a	186,187	O	O
different	188,197	O	O
reduction	198,207	O	B-Process
to	208,210	O	O
a	211,212	O	O
system	213,219	O	O
from	220,224	O	O
the	225,228	O	O
previously	229,239	O	O
used	240,244	O	O
one	245,248	O	O
.	248,249	O	O

We	250,252	O	O
find	253,257	O	O
the	258,261	O	B-Data
Levi	262,266	O	I-Data
conditions	267,277	O	I-Data
such	278,282	O	O
that	283,287	O	O
the	288,291	O	B-Data
C∞	292,294	O	I-Data
well	295,299	O	I-Data
-	299,300	O	I-Data
posedness	300,309	O	I-Data
continues	310,319	O	O
to	320,322	O	O
hold	323,327	O	O
.	327,328	O	O

