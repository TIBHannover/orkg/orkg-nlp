-DOCSTART- (S0167610513002729)

Dispersion	0,10	O	B-Process
in	11,13	O	O
the	14,17	O	B-Material
near	18,22	O	I-Material
-	22,23	O	I-Material
field	23,28	O	I-Material
region	29,35	O	I-Material
of	36,38	O	O
localised	39,48	O	B-Material
releases	49,57	O	I-Material
in	58,60	O	O
urban	61,66	O	B-Material
areas	67,72	O	I-Material
is	73,75	O	O
difficult	76,85	O	O
to	86,88	O	O
predict	89,96	O	B-Process
because	97,104	O	O
of	105,107	O	O
the	108,111	O	B-Process
strong	112,118	O	I-Process
influence	119,128	O	I-Process
of	129,131	O	O
individual	132,142	O	B-Material
buildings	143,152	O	I-Material
.	152,153	O	O

Effects	154,161	O	B-Process
include	162,169	O	O
upstream	170,178	O	B-Process
dispersion	179,189	O	I-Process
,	189,190	O	O
trapping	191,199	O	B-Process
of	200,202	O	O
material	203,211	O	B-Material
into	212,216	O	O
building	217,225	O	B-Material
wakes	226,231	O	I-Material
and	232,235	O	O
enhanced	236,244	O	B-Process
concentration	245,258	O	I-Process
fluctuations	259,271	O	I-Process
.	271,272	O	O

As	273,275	O	O
a	276,277	O	O
result	278,284	O	O
,	284,285	O	O
concentration	286,299	O	B-Data
patterns	300,308	O	I-Data
are	309,312	O	O
highly	313,319	O	B-Process
variable	320,328	O	I-Process
in	329,331	O	O
time	332,336	O	B-Data
and	337,340	O	I-Data
mean	341,345	O	I-Data
profiles	346,354	O	I-Data
in	355,357	O	O
the	358,361	O	B-Material
near	362,366	O	I-Material
field	367,372	O	I-Material
are	373,376	O	O
strongly	377,385	O	B-Data
non	386,389	O	I-Data
-	389,390	O	I-Data
Gaussian	390,398	O	I-Data
.	398,399	O	O

These	400,405	O	B-Data
aspects	406,413	O	I-Data
of	414,416	O	O
near	417,421	O	B-Process
-	421,422	O	I-Process
field	422,427	O	I-Process
dispersion	428,438	O	I-Process
are	439,442	O	O
documented	443,453	O	B-Process
by	454,456	O	O
analysing	457,466	O	B-Process
data	467,471	O	O
from	472,476	O	O
direct	477,483	O	B-Material
numerical	484,493	O	I-Material
simulations	494,505	O	I-Material
in	506,508	O	O
arrays	509,515	O	B-Material
of	516,518	O	I-Material
building	519,527	O	I-Material
-	527,528	O	I-Material
like	528,532	O	I-Material
obstacles	533,542	O	I-Material
and	543,546	O	O
are	547,550	O	O
related	551,558	O	O
to	559,561	O	O
the	562,565	O	B-Data
underlying	566,576	O	I-Data
flow	577,581	O	I-Data
structure	582,591	O	I-Data
.	591,592	O	O

The	593,596	O	B-Data
mean	597,601	O	I-Data
flow	602,606	O	I-Data
structure	607,616	O	I-Data
around	617,623	O	O
the	624,627	O	B-Material
buildings	628,637	O	I-Material
is	638,640	O	O
found	641,646	O	O
to	647,649	O	O
exert	650,655	O	O
a	656,657	O	B-Process
strong	658,664	O	I-Process
influence	665,674	O	I-Process
over	675,679	O	O
the	680,683	O	B-Process
dispersion	684,694	O	I-Process
of	695,697	O	O
material	698,706	O	B-Material
in	707,709	O	I-Material
the	710,713	O	I-Material
near	714,718	O	I-Material
field	719,724	O	I-Material
.	724,725	O	O

Diverging	726,735	O	B-Process
streamlines	736,747	O	I-Process
around	748,754	O	O
buildings	755,764	O	B-Material
enhance	765,772	O	B-Process
lateral	773,780	O	B-Process
dispersion	781,791	O	I-Process
.	791,792	O	O

Entrainment	793,804	O	B-Process
of	805,807	O	O
material	808,816	O	O
into	817,821	O	O
building	822,830	O	B-Material
wakes	831,836	O	I-Material
in	837,839	O	O
the	840,843	O	B-Material
very	844,848	O	I-Material
near	849,853	O	I-Material
field	854,859	O	I-Material
gives	860,865	O	O
rise	866,870	O	O
to	871,873	O	O
secondary	874,883	O	B-Material
sources	884,891	O	I-Material
,	891,892	O	O
which	893,898	O	O
then	899,903	O	O
affect	904,910	O	O
the	911,914	O	B-Data
subsequent	915,925	O	I-Data
dispersion	926,936	O	I-Data
pattern	937,944	O	I-Data
.	944,945	O	O

High	946,950	O	B-Data
levels	951,957	O	I-Data
of	958,960	O	O
concentration	961,974	O	B-Process
fluctuations	975,987	O	I-Process
are	988,991	O	O
also	992,996	O	O
found	997,1002	O	O
in	1003,1005	O	O
this	1006,1010	O	B-Material
very	1011,1015	O	I-Material
near	1016,1020	O	I-Material
field	1021,1026	O	I-Material
;	1026,1027	O	O
the	1028,1031	O	B-Data
fluctuation	1032,1043	O	I-Data
intensity	1044,1053	O	I-Data
is	1054,1056	O	O
of	1057,1059	O	O
order	1060,1065	O	B-Data
2	1066,1067	O	I-Data
to	1068,1070	O	I-Data
5	1071,1072	O	I-Data
.	1072,1073	O	O

Highlights	1074,1084	O	O
•	1085,1086	O	O

We	1087,1089	O	O
document	1090,1098	O	B-Process
key	1099,1102	O	B-Data
aspects	1103,1110	O	I-Data
of	1111,1113	O	O
near	1114,1118	O	B-Process
-	1118,1119	O	I-Process
field	1119,1124	O	I-Process
dispersion	1125,1135	O	I-Process
in	1136,1138	O	O
urban	1139,1144	O	B-Material
areas	1145,1150	O	I-Material
using	1151,1156	O	O
DNS	1157,1160	O	B-Method
data	1161,1165	O	O
.	1165,1166	O	O

•	1167,1168	O	O

We	1169,1171	O	O
relate	1172,1178	O	O
these	1179,1184	O	B-Data
dispersion	1185,1195	O	I-Data
features	1196,1204	O	I-Data
to	1205,1207	O	O
the	1208,1211	O	B-Data
underlying	1212,1222	O	I-Data
flow	1223,1227	O	I-Data
structure	1228,1237	O	I-Data
.	1237,1238	O	O

•	1239,1240	O	O

The	1241,1244	O	B-Process
effects	1245,1252	O	I-Process
of	1253,1255	O	O
wind	1256,1260	O	B-Process
direction	1261,1270	O	I-Process
,	1270,1271	O	O
building	1272,1280	O	B-Material
layout	1281,1287	O	I-Material
and	1288,1291	O	O
source	1292,1298	O	B-Material
location	1299,1307	O	I-Material
are	1308,1311	O	O
examined	1312,1320	O	B-Process
.	1320,1321	O	O

•	1322,1323	O	O

Secondary	1324,1333	O	B-Material
sources	1334,1341	O	I-Material
in	1342,1344	O	O
the	1345,1348	O	B-Material
near	1349,1353	O	I-Material
field	1354,1359	O	I-Material
have	1360,1364	O	O
a	1365,1366	O	O
strong	1367,1373	O	O
effect	1374,1380	O	O
on	1381,1383	O	O
the	1384,1387	O	B-Material
plume	1388,1393	O	I-Material
spread	1394,1400	O	I-Material
.	1400,1401	O	O

•	1402,1403	O	O

Concentration	1404,1417	O	B-Data
means	1418,1423	O	I-Data
and	1424,1427	O	O
fluctuations	1428,1440	O	B-Process
are	1441,1444	O	O
mapped	1445,1451	O	B-Process
and	1452,1455	O	O
their	1456,1461	O	B-Data
magnitudes	1462,1472	O	I-Data
quantified	1473,1483	O	B-Process
.	1483,1484	O	O

-DOCSTART- (S0378383913001567)

In	0,2	O	O
this	3,7	O	O
paper	8,13	O	O
we	14,16	O	O
propose	17,24	O	O
an	25,27	O	B-Process
integral	28,36	O	I-Process
form	37,41	O	I-Process
of	42,44	O	O
the	45,48	O	B-Process
fully	49,54	O	I-Process
non	55,58	O	I-Process
-	58,59	O	I-Process
linear	59,65	O	I-Process
Boussinesq	66,76	O	I-Process
equations	77,86	O	I-Process
in	87,89	O	O
contravariant	90,103	O	B-Data
formulation	104,115	O	I-Data
,	115,116	O	O
in	117,119	O	O
which	120,125	O	O
Christoffel	126,137	O	B-Data
symbols	138,145	O	I-Data
are	146,149	O	O
avoided	150,157	O	O
,	157,158	O	O
in	159,161	O	O
order	162,167	O	O
to	168,170	O	O
simulate	171,179	O	B-Process
wave	180,184	O	B-Process
transformation	185,199	O	I-Process
phenomena	200,209	O	O
,	209,210	O	O
wave	211,215	O	B-Process
breaking	216,224	O	I-Process
and	225,228	O	O
nearshore	229,238	O	B-Process
currents	239,247	O	I-Process
in	248,250	O	O
computational	251,264	O	B-Material
domains	265,272	O	I-Material
representing	273,285	O	O
the	286,289	O	B-Data
complex	290,297	O	I-Data
morphology	298,308	O	I-Data
of	309,311	O	O
real	312,316	O	B-Material
coastal	317,324	O	I-Material
regions	325,332	O	I-Material
.	332,333	O	O

Following	334,343	O	O
the	344,347	O	O
approach	348,356	O	O
proposed	357,365	O	O
by	366,368	O	O
Chen	369,373	O	O
(	374,375	O	O
2006	375,379	O	O
)	379,380	O	O
,	380,381	O	O
the	382,385	O	B-Process
motion	386,392	O	I-Process
equations	393,402	O	I-Process
retain	403,409	O	O
the	410,413	O	O
term	414,418	O	O
related	419,426	O	O
to	427,429	O	O
the	430,433	O	O
approximation	434,447	O	O
to	448,450	O	O
the	451,454	O	B-Data
second	455,461	O	I-Data
order	462,467	O	I-Data
of	468,470	O	O
the	471,474	O	B-Process
vertical	475,483	O	I-Process
vorticity	484,493	O	I-Process
.	493,494	O	O

A	495,496	O	B-Method
new	497,500	O	I-Method
Upwind	501,507	O	I-Method
Weighted	508,516	O	I-Method
Essentially	517,528	O	I-Method
Non	529,532	O	I-Method
-	532,533	O	I-Method
Oscillatory	533,544	O	I-Method
scheme	545,551	O	I-Method
for	552,555	O	O
the	556,559	O	O
solution	560,568	O	O
of	569,571	O	O
the	572,575	O	B-Process
fully	576,581	O	I-Process
non	582,585	O	I-Process
-	585,586	O	I-Process
linear	586,592	O	I-Process
Boussinesq	593,603	O	I-Process
equations	604,613	O	I-Process
on	614,616	O	O
generalised	617,628	O	B-Material
curvilinear	629,640	O	I-Material
coordinate	641,651	O	I-Material
systems	652,659	O	I-Material
is	660,662	O	O
proposed	663,671	O	O
.	671,672	O	O

The	673,676	O	B-Method
equations	677,686	O	I-Method
are	687,690	O	O
rearranged	691,701	O	O
in	702,704	O	O
order	705,710	O	O
to	711,713	O	O
solve	714,719	O	O
them	720,724	O	O
by	725,727	O	O
a	728,729	O	B-Method
high	730,734	O	I-Method
resolution	735,745	O	I-Method
hybrid	746,752	O	I-Method
finite	753,759	O	I-Method
volume	760,766	O	I-Method
-	766,767	O	I-Method
finite	767,773	O	I-Method
difference	774,784	O	I-Method
scheme	785,791	O	I-Method
.	791,792	O	O

The	793,796	O	B-Data
conservative	797,809	O	I-Data
part	810,814	O	I-Data
of	815,817	O	O
the	818,821	O	B-Process
above	822,827	O	I-Process
-	827,828	O	I-Process
mentioned	828,837	O	I-Process
equations	838,847	O	I-Process
,	847,848	O	O
consisting	849,859	O	O
of	860,862	O	O
the	863,866	O	B-Data
convective	867,877	O	I-Data
terms	878,883	O	I-Data
and	884,887	O	O
the	888,891	O	B-Data
terms	892,897	O	I-Data
related	898,905	O	O
to	906,908	O	O
the	909,912	O	B-Process
free	913,917	O	I-Process
surface	918,925	O	I-Process
elevation	926,935	O	I-Process
,	935,936	O	O
is	937,939	O	O
discretised	940,951	O	B-Process
by	952,954	O	O
a	955,956	O	B-Method
high	957,961	O	I-Method
-	961,962	O	I-Method
order	962,967	O	I-Method
shock	968,973	O	I-Method
-	973,974	O	I-Method
capturing	974,983	O	I-Method
finite	984,990	O	I-Method
volume	991,997	O	I-Method
scheme	998,1004	O	I-Method
in	1005,1007	O	O
which	1008,1013	O	O
an	1014,1016	O	B-Material
exact	1017,1022	O	I-Material
Riemann	1023,1030	O	I-Material
solver	1031,1037	O	I-Material
is	1038,1040	O	O
involved	1041,1049	O	O
;	1049,1050	O	O
dispersive	1051,1061	O	B-Data
terms	1062,1067	O	I-Data
and	1068,1071	O	O
the	1072,1075	O	O
term	1076,1080	O	O
related	1081,1088	O	O
to	1089,1091	O	O
the	1092,1095	O	B-Process
approximation	1096,1109	O	I-Process
to	1110,1112	O	O
the	1113,1116	O	B-Data
second	1117,1123	O	I-Data
order	1124,1129	O	I-Data
of	1130,1132	O	O
the	1133,1136	O	B-Process
vertical	1137,1145	O	I-Process
vorticity	1146,1155	O	I-Process
are	1156,1159	O	O
discretised	1160,1171	O	B-Process
by	1172,1174	O	O
a	1175,1176	O	B-Method
cell	1177,1181	O	I-Method
-	1181,1182	O	I-Method
centred	1182,1189	O	I-Method
finite	1190,1196	O	I-Method
difference	1197,1207	O	I-Method
scheme	1208,1214	O	I-Method
.	1214,1215	O	O

The	1216,1219	O	B-Method
shock	1220,1225	O	I-Method
-	1225,1226	O	I-Method
capturing	1226,1235	O	I-Method
method	1236,1242	O	I-Method
makes	1243,1248	O	O
it	1249,1251	O	O
possible	1252,1260	O	O
to	1261,1263	O	O
intrinsically	1264,1277	O	O
model	1278,1283	O	O
the	1284,1287	O	B-Process
wave	1288,1292	O	I-Process
breaking	1293,1301	O	I-Process
,	1301,1302	O	O
therefore	1303,1312	O	O
no	1313,1315	O	O
additional	1316,1326	O	O
terms	1327,1332	O	O
are	1333,1336	O	O
needed	1337,1343	O	O
to	1344,1346	O	O
take	1347,1351	O	O
into	1352,1356	O	O
account	1357,1364	O	O
the	1365,1368	O	B-Process
breaking	1369,1377	O	I-Process
related	1378,1385	O	I-Process
energy	1386,1392	O	I-Process
dissipation	1393,1404	O	I-Process
in	1405,1407	O	O
the	1408,1411	O	B-Material
surf	1412,1416	O	I-Material
zone	1417,1421	O	I-Material
.	1421,1422	O	O

The	1423,1426	O	B-Material
model	1427,1432	O	I-Material
is	1433,1435	O	O
verified	1436,1444	O	B-Process
against	1445,1452	O	O
several	1453,1460	O	O
benchmark	1461,1470	O	B-Data
tests	1471,1476	O	I-Data
,	1476,1477	O	O
and	1478,1481	O	O
the	1482,1485	O	O
results	1486,1493	O	O
are	1494,1497	O	O
compared	1498,1506	O	O
with	1507,1511	O	O
experimental	1512,1524	O	B-Data
,	1524,1525	O	I-Data
theoretical	1526,1537	O	I-Data
and	1538,1541	O	I-Data
alternative	1542,1553	O	I-Data
numerical	1554,1563	O	I-Data
solutions	1564,1573	O	I-Data
.	1573,1574	O	O

Highlights	1575,1585	O	O
•	1586,1587	O	O

We	1588,1590	O	O
propose	1591,1598	O	O
a	1599,1600	O	B-Data
contravariant	1601,1614	O	I-Data
formulation	1615,1626	O	I-Data
of	1627,1629	O	O
the	1630,1633	O	B-Process
fully	1634,1639	O	I-Process
non	1640,1643	O	I-Process
-	1643,1644	O	I-Process
linear	1644,1650	O	I-Process
Boussinesq	1651,1661	O	I-Process
equations	1662,1671	O	I-Process
.	1671,1672	O	O

•	1673,1674	O	O

The	1675,1678	O	O
presented	1679,1688	O	O
contravariant	1689,1702	O	B-Data
formulation	1703,1714	O	I-Data
is	1715,1717	O	O
free	1718,1722	O	O
of	1723,1725	O	O
Christoffel	1726,1737	O	B-Data
symbols	1738,1745	O	I-Data
.	1745,1746	O	O

•	1747,1748	O	O

The	1749,1752	O	B-Method
equations	1753,1762	O	I-Method
retain	1763,1769	O	O
the	1770,1773	O	B-Data
approximation	1774,1787	O	I-Data
to	1788,1790	O	O
the	1791,1794	O	B-Data
second	1795,1801	O	I-Data
order	1802,1807	O	I-Data
of	1808,1810	O	O
the	1811,1814	O	B-Process
vertical	1815,1823	O	I-Process
vorticity	1824,1833	O	I-Process
.	1833,1834	O	O

•	1835,1836	O	O

We	1837,1839	O	O
present	1840,1847	O	O
a	1848,1849	O	B-Method
new	1850,1853	O	I-Method
Upwind	1854,1860	O	I-Method
Weighted	1861,1869	O	I-Method
Essentially	1870,1881	O	I-Method
Non	1882,1885	O	I-Method
-	1885,1886	O	I-Method
Oscillatory	1886,1897	O	I-Method
scheme	1898,1904	O	I-Method
.	1904,1905	O	O

•	1906,1907	O	O

The	1908,1911	O	B-Method
shock	1912,1917	O	I-Method
-	1917,1918	O	I-Method
capturing	1918,1927	O	I-Method
scheme	1928,1934	O	I-Method
allows	1935,1941	O	O
an	1942,1944	O	B-Material
explicit	1945,1953	O	I-Material
simulation	1954,1964	O	I-Material
of	1965,1967	O	O
the	1968,1971	O	B-Process
wave	1972,1976	O	I-Process
breaking	1977,1985	O	I-Process
.	1985,1986	O	O

