from standoffreader import parse_entities, create_spacy_tokenizer, process_standoff_files
import glob
import os
import itertools
import pandas as pd
import numpy as np

def print_stats(path, out):
    '''Number of entitities for a specific label and domain'''
    with open(out, "w", encoding="utf-8") as out_f:
        tags_per_domain = dict()
        for d in glob.glob(f"{path}/*/"):
            domain = os.path.basename(os.path.normpath(d))
            tags_per_domain[domain] = []
            for ann in glob.glob(f'{d}/*.ann'):

                with open(ann, "r", encoding="utf--8") as ann_f:
                    ann_txt = ann_f.read()
                entities = parse_entities(ann_txt)
                for e in entities:
                    tags_per_domain[domain].append(e.label)

            #for each domain
            sum = 0
            for x, y in itertools.groupby(sorted(tags_per_domain[domain])):
                sum += len(list(y))
            for x, y in itertools.groupby(sorted(tags_per_domain[domain])):
                num = len(list(y))
                out_f.write(f'{domain},{x},{num},{round(num / sum * 100, 2)}\n')

        #overall
        tags = [t for domain_tags in tags_per_domain.values() for t in domain_tags]
        sum = 0
        for x, y in itertools.groupby(sorted(tags)):
            sum += len(list(y))
        for x, y in itertools.groupby(sorted(tags)):
            num = len(list(y))
            out_f.write(f'overall,{x},{num},{round(num / sum * 100, 2)}\n')


def print_unique_instances_per_label(path, out):
    '''Number of unique entitities for a specific label and domain'''
    with open(out, "w", encoding="utf-8") as out_f:
        tags_per_domain = dict()
        for d in glob.glob(f"{path}/*/"):
            domain = os.path.basename(os.path.normpath(d))
            tags_per_domain[domain] = set()
            for ann in glob.glob(f'{d}/*.ann'):

                with open(ann, "r", encoding="utf--8") as ann_f:
                    ann_txt = ann_f.read()
                entities = parse_entities(ann_txt)
                for e in entities:
                    tags_per_domain[domain].add(e.text)

        for d in tags_per_domain.keys():
                out_f.write(f'{d},{len(tags_per_domain[d])}\n')


def print_label_instances(path, out):

    with open(out, "w", encoding="utf-8") as out_f:
        for d in glob.glob(f"{path}/*/"):
            domain = os.path.basename(os.path.normpath(d))
            label_instances = dict()

            for ann in glob.glob(f'{d}/*.ann'):
                with open(ann, "r", encoding="utf--8") as ann_f:
                    ann_txt = ann_f.read()
                entities = parse_entities(ann_txt)
                for e in entities:
                    if e.label not in label_instances :
                        label_instances [e.label] = []
                    label_instances [e.label].append(e.text)

            for label, instances in label_instances .items():
                for instance in instances:
                    out_f.write(f'{domain},{label},{instance}\n')

def print_token_stats(path, out):
    ignore_entities = ["Result", "Task", "Object"]
    spacy_nlp = create_spacy_tokenizer()
    rows = []
    for d in glob.glob(f"{path}/*/"):
        domain = os.path.basename(os.path.normpath(d))
        print(domain)
        docs = process_standoff_files(d, keep_only_one_label=True, spacy_nlp=spacy_nlp, ignore_entities=ignore_entities)
        num_docs = len(docs)
        num_sentences = 0
        num_tokens = 0
        for d in docs:
            num_sentences += len(d.sentences)
            for sentence in d.sentences:
                num_tokens += len(sentence)
        rows.append([domain, "abstracts", num_docs])
        rows.append([domain, "sentences", num_sentences])
        rows.append([domain, "tokens", num_tokens])

    stats = pd.DataFrame(rows, columns=["domain", "stat", "count"])

    stats = pd.pivot_table(stats, index=["stat"], values=["count"], columns=["domain"], margins=True, aggfunc=np.sum)

    # remove the bottom margin
    stats = stats.iloc[:-1, :]

    stats.to_csv(out)


print_stats("data/dataset_v2/origin", "data/dataset_v2/stm_stats.csv")
print_label_instances("data/dataset_v2/origin", "data/dataset_v2/stm_label_instances.csv")
print_unique_instances_per_label("data/dataset_v2/origin", "data/dataset_v2/stm_unique_stats.csv")
print_token_stats("data/dataset_v2/origin", "data/dataset_v2/token_stats.csv")

