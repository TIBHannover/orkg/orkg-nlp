## The STEM-ECR Dataset: 
### Grounding Scientific Entity References in STEM Scholarly Content to Authoritative Encyclopedic and Lexicographic Sources.
The STEM ECR v1.0 dataset has been developed to provide a benchmark for the evaluation of scientific entity extraction, classification, and resolution tasks in a domain-independent fashion.


## What this repository contains?

The dataset is organised in the following directories:
* `readme.md': this file.
* `/scientific-entity-annotations': Contains annotations for Process, Material, Method, and Data scientific entities in a STEM dataset.
* `/scientific-entity-resolution': Annotations for the STEM dataset scientific entities with Entity Linking (EL) annotations to Wikipedia and Word Sense Disambiguation (WSD) annotations to Wiktionary.



### Annotation Guidelines
The annotation guidelines that supported the creation of this corpus at [here](annotation-guidelines-for-scientific-linked-data.pdf)

### Dataset size
The dataset materials are 3.3MB

### Cite this as
D'Souza, J., Hoppe, A., Brack, A., Jaradeh, M., Auer, S., & Ewerth, R. (2020). [The STEM-ECR Dataset: Grounding Scientific Entity References in STEM Scholarly Content to Authoritative Encyclopedic and Lexicographic Sources](https://www.aclweb.org/anthology/2020.lrec-1.268/). In Proceedings of The 12th Language Resources and Evaluation Conference (pp. 2192–2203). European Language Resources Association.

## History
STEM ECR v1.0 is developed by Jennifer D’Souza, Anett Hoppe, Arthur Brack, Mohamad Yaser Jaradeh, Soren Auer, and Ralph Ewerth.

## Contact us

If you have questions, or you would like to change or contribute to this resource, please contact Dr. Jennifer D'Souza (jennifer.dsouza at tib.eu).


## Useful Links
* https://github.com/elsevierlabs/OA-STM-Corpus/
* https://brat.nlplab.org/
* https://dumps.wikimedia.org/enwiki/20190920/
* https://dumps.wikimedia.org/enwiktionary/20190920/
* https://dkpro.github.io/dkpro-jwpl/
* https://dkpro.github.io/dkpro-jwktl/
* https://orkg.org/orkg
