# TDM-Extraction Service

This directory includes supplemental material for preparing the files
required to run the TDM-Extraction service as a standalone service.

## Prerequisites 

* Fine-tuned XLNet model `/models/orkgnlp-tdm-extraction-xlnet.pt`.

## How to Run

You can run the `to_pretrained.py` script to:

* save the fine-tuned model in a transformers-pretrained-model-like format. 

by running the following:

```commandline
git clone https://gitlab.com/TIBHannover/orkg/nlp/orkg-nlp-experiments.git
git cd orkg-nlp-experiments/orkg_tdm_extraction
pip install -r requirements.txt
python to_pretrained.py
```

## What next ? 

The output files can be uploaded to Huggingface and used by `orkgnlp`!